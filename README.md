# Darkest Dungeon Cheat Sheet

A small web app to provide an **interactive cheat sheet** for the _Darkest
Dungeon_. Select an _expedition location_ along with it's _length_ and get
suggestions for the amount of _provisions_ to bring and how to use them on the
possible _curios_.

1. Use Python [Scrapy][] to spider the [Darkest Dungeon Wiki][dd-wiki] and dump
   the results to a [TOML][] file.

2. Use the data with [Vue][] for a small **Cheat Sheet** web app.

[dd-wiki]: https://darkestdungeon.gamepedia.com/
[scrapy]: https://scrapy.org/
[toml]: https://toml.io/
[vue]: https://v3.vuejs.org/
